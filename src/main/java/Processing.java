import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import static java.util.function.Function.identity;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingLong;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import javafx.util.Pair;


public class Processing {

    private String inputFile;
    ArrayList<MonitoredData> list;

    public Processing(String inputFile) {
        this.inputFile = inputFile;
        readActivities();
    }

    private void readActivities() {
        try {
            Stream<String> stream = Files.lines(Paths.get(inputFile));  // obtains a stream with each line in the file
            list = stream
                    .map(s -> new MonitoredData(s))
                    .collect(toCollection(ArrayList::new));    //stream -> arraylist
        } catch (IOException ex) {
            Logger.getLogger(Processing.class.getName()).log(Level.SEVERE, "IO Exception", ex);
        } catch (Exception ex) {
            Logger.getLogger(Processing.class.getName()).log(Level.SEVERE, "Exception", ex);
        }
    }

    /**
     * Count how many days of monitored data appears in the log.
     *
     * @return number of days
     */
    public long noOfDays() {
        long nr = list.stream()
                .map(a -> a.getEndDate())
                .distinct()
                .count();
        return nr;
    }

    public void displayActivities(String fileName) {
        try {
            PrintWriter pw = new PrintWriter(Files.newBufferedWriter(Paths.get(fileName)));
            list.stream()
                    .forEach(pw::println);
        } catch (IOException ex) {
            Logger.getLogger(Processing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Count how many times each activity has appeared over the entire
     * monitoring period.
     *
     * @return the mapping of activities to their count.
     */

    //https://stackoverflow.com/questions/34859884/stream-collectgroupingbyidentity-counting-and-then-sort-the-result-by-va
    public Map<String, Long> countActivitiesOverPeriod() {
        Map<String, Long> map;
        map = list.stream()
                .map(MonitoredData::getActivity)
                .collect(groupingBy(identity(), counting()));
        return map;
    }

    public void writeActivitiesOverPeriod(String fileName) {

        Map<String, Long> map = countActivitiesOverPeriod();
        try {
            // din https://stackoverflow.com/questions/32054180/java-8-stream-to-file
            PrintWriter pw = new PrintWriter(Files.newBufferedWriter(Paths.get(fileName)));//first creates bufferedWriter object -> to printWriter ()
            map.entrySet().stream()
                    .forEach(entry -> pw.println(entry.getKey() + " " + entry.getValue()));
            pw.flush(); // it didn't write the data to file without this
        } catch (IOException ex) {
            Logger.getLogger(Processing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
///////////////////////////////
    /**
     * Count how many times has appeared each activity for each day over the
     * monitoring period
     *
     * @return
     */
    public Map<Pair<String, Date>, Long> countActivitiesPerDay() { //runtime error for Map because of same key
        //Map<Map.Entry<String, Date>, Long> map;
        /*List<Pair<String, Date>> activitiesPerDays = list.stream()
                .map(a -> new Pair<>(a.getActivity(), a.getStartDate()))
                .collect(toList());
        Map<Pair<String, Date>, Long> map = activitiesPerDays.stream()
                .collect(groupingBy(identity(), counting()));
                */
        //https://www.mkyong.com/java8/java-8-collectors-groupingby-and-mapping-example/
        Map<Pair<String, Date>, Long> map = list.stream()
                .map(a -> new Pair<>(a.getActivity(), a.getStartDate())) //stream of pairs (activty, startDate)
                .collect(groupingBy(identity(), counting())); //a.getActiviti, date -> counts

        return map;
    }



    public void writeActivitiesPerDay(String fileName) {
        Map<Pair<String, Date>, Long> map = countActivitiesPerDay();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            // din https://stackoverflow.com/questions/32054180/java-8-stream-to-file
            PrintWriter pw = new PrintWriter(Files.newBufferedWriter(Paths.get(fileName)));
            map.entrySet().stream()
                    .forEach(entry -> pw.println(format.format(entry.getKey().getValue())
                            + " " + entry.getKey().getKey()
                            + " " + entry.getValue()));
            pw.flush(); // it didn't write the data to file without this //clears buffer
        } catch (IOException ex) {
            Logger.getLogger(Processing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Compute how long each activity has lasted over the entire monitoring
     * period (entire duration over the monitoring period).
     *
     * @return the mapping of activities to their duration.
     */
    public List<Pair<String, Long>> computeActivityDurationOverPeriod() {
        // winterbe.com/posts/2014/07/31/java8-stream-tutorial-examples/ //
        Map<String, List<MonitoredData>> activityByType = list.stream()
                .collect(groupingBy(MonitoredData::getActivity));

        List<Pair<String, Long>> map = activityByType.entrySet().stream()
                .map(entry -> {
                    String name = entry.getKey();
                    List<MonitoredData> activityList = entry.getValue();
                    Long val = activityList.stream()
                            .collect(summingLong(a -> a.getSeconds())); 
                    return new Pair<>(name, val);
                })
                .collect(toList());
        return map;
    }

    public void writeActivityDurationOverPeriod(String fileName) {
        List<Pair<String, Long>> activities = computeActivityDurationOverPeriod();
        try {
            // din https://stackoverflow.com/questions/32054180/java-8-stream-to-file
            PrintWriter pw = new PrintWriter(Files.newBufferedWriter(Paths.get(fileName)));
            activities.stream()
                    .forEach(entry -> {
                        long secs = entry.getValue();
                        long hours = secs / 3600;
                        secs %= 3600;
                        long mins = secs / 60;
                        secs %= 60;
                        pw.println(entry.getKey()
                                + " " + hours + ":" + mins + ":" + secs);
                    });
            pw.flush(); // it didn't write the data to file without this
        } catch (IOException ex) {
            Logger.getLogger(Processing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Obtains the list of activities that have at least 90% of the monitoring
     * records with duration less than 5 minutes
     */
    public List<String> filterActivities() {
        Map<String, List<MonitoredData>> activityByType = list.stream()
                .collect(groupingBy(MonitoredData::getActivity));

        List<String> filteredActivit = activityByType.entrySet().stream()
                .filter(entry -> {
                    List<MonitoredData> activities = entry.getValue();
                    int size = activities.size();
                    List<MonitoredData> filtered = activities.stream()
                                    .filter(a -> a.getSeconds() < 300) // duration less than 5 minutes
                                    .collect(toList());
                    return filtered.size() >= 0.9 * size;
                })
                .map(entry->entry.getKey())
                .distinct()
                .collect(toList());

        return filteredActivit;
    }

    public void writeFilteredActivities(String fileName) {
        List<String> filteredList = filterActivities();
        try {
            // din https://stackoverflow.com/questions/32054180/java-8-stream-to-file
            PrintWriter pw = new PrintWriter(Files.newBufferedWriter(Paths.get(fileName)));
            filteredList.stream().forEach(pw::println);
            pw.flush(); // it didn't write the data to file without this
        } catch (IOException ex) {
            Logger.getLogger(Processing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
