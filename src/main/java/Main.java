import java.text.ParseException;

public class Main {

    public static void main(String[] args) throws ParseException {
        Processing o = new Processing("Activities.txt");
        System.out.println("Number of days: " + o.noOfDays()); //task 1
        o.writeActivitiesOverPeriod("ActivitiesOverPeriod.txt");//task2
        o.writeActivitiesPerDay("ActivitiesPerDay.txt");//task 3
        o.displayActivities("DurationOnThatLine.txt");//task 4
        o.writeActivityDurationOverPeriod("ActivityDurations.txt");//task 5
        o.writeFilteredActivities("VeryShortActivities.txt");//task 6
    }
}
