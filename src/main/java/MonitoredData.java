import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MonitoredData {

    private Date startTime;
    private Date endTime;
    private String activity;
    private long noOfSeconds;

    public MonitoredData(String s) {
        // assume s contains the data for an activity, for example:
        // 2011-11-28 02:27:59		2011-11-28 10:18:11		Sleeping
        String[] data = s.split("[\\t]+");   // tab is a separator; + means any number of tabs
        try {
            if (data.length != 3) {
                throw new ParseException("String not in the right format!", 0);
            }
            startTime = convertDate(data[0]);
            endTime = convertDate(data[1]);
            noOfSeconds = (endTime.getTime()-startTime.getTime())/1000;     // number of milliseconds converted to seconds
            activity = data[2].trim();
        } catch (ParseException ex) {
            Logger.getLogger(MonitoredData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String toString() {
        return startTime + " " + endTime + " Activity: " + activity + 
                "("+ noOfSeconds + " seconds)";
    }

    public String getActivity() {
        return activity;
    }
    
    public long getSeconds() {
        return noOfSeconds;
    }

    public Date getEndDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dateWithoutTime = null;
        try {
            dateWithoutTime = sdf.parse(sdf.format(endTime));
        } catch (ParseException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dateWithoutTime;
    }

        public Date getStartDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dateWithoutTime = null;
        try {
            dateWithoutTime = sdf.parse(sdf.format(startTime));
        } catch (ParseException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dateWithoutTime;
    }

    private Date convertDate(String s) throws ParseException {
        // s should start with a date in the form   2011-11-28 02:27:59  and it may contain other text       
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // assumes date of the form 2011-11-28 10:03:55
        return format.parse(s);
    }
}
